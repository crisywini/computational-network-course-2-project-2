package co.uniquindio.redesdecomputadores2.infraestructure.controller;

import co.uniquindio.redesdecomputadores2.application.service.*;
import co.uniquindio.redesdecomputadores2.domain.model.SavingsAccount;

import java.io.IOException;
import java.util.List;

public class SavingsAccountController {

    private final OpenAccountApplicationService openAccountApplicationService;
    private final OpenPocketApplicationService openPocketApplicationService;
    private final CancelPocketApplicationService cancelPocketApplicationService;
    private final CancelSavingsAccountApplicationService cancelSavingsAccountApplicationService;
    private final DepositMoneyIntoSavingsAccountApplicationService depositMoneyIntoSavingsAccountApplicationService;
    private final WithdrawMoneyFromSavingsAccountApplicationService withdrawMoneyFromSavingsAccountApplicationService;
    private final TransferMoneyToAPocketApplicationService transferMoneyToAPocketApplicationService;
    private final CheckSavingsAccountBalanceApplicationService checkSavingsAccountBalanceApplicationService;
    private final GetTransactionsApplicationService getTransactionsApplicationService;

    public SavingsAccountController(OpenAccountApplicationService openAccountApplicationService,
                                    OpenPocketApplicationService openPocketApplicationService,
                                    CancelPocketApplicationService cancelPocketApplicationService,
                                    CancelSavingsAccountApplicationService cancelSavingsAccountApplicationService,
                                    DepositMoneyIntoSavingsAccountApplicationService depositMoneyIntoSavingsAccountApplicationService,
                                    WithdrawMoneyFromSavingsAccountApplicationService withdrawMoneyFromSavingsAccountApplicationService,
                                    TransferMoneyToAPocketApplicationService transferMoneyToAPocketApplicationService,
                                    CheckSavingsAccountBalanceApplicationService checkSavingsAccountBalanceApplicationService,
                                    GetTransactionsApplicationService getTransactionsApplicationService) {
        this.openAccountApplicationService = openAccountApplicationService;
        this.openPocketApplicationService = openPocketApplicationService;
        this.cancelPocketApplicationService = cancelPocketApplicationService;
        this.cancelSavingsAccountApplicationService = cancelSavingsAccountApplicationService;
        this.depositMoneyIntoSavingsAccountApplicationService = depositMoneyIntoSavingsAccountApplicationService;
        this.withdrawMoneyFromSavingsAccountApplicationService = withdrawMoneyFromSavingsAccountApplicationService;
        this.transferMoneyToAPocketApplicationService = transferMoneyToAPocketApplicationService;
        this.checkSavingsAccountBalanceApplicationService = checkSavingsAccountBalanceApplicationService;
        this.getTransactionsApplicationService = getTransactionsApplicationService;
    }

    public void openSavingsAccount(SavingsAccount savingsAccount) {
        openAccountApplicationService.run(savingsAccount);
    }

    public void openPocket(String idMainAccount) {
        openPocketApplicationService.run(idMainAccount);
    }

    public void cancelPocket(String idPocket) {
        cancelPocketApplicationService.run(idPocket);
    }

    public void cancelSavingsAccount(String idSavingsAccount) {
        cancelSavingsAccountApplicationService.run(idSavingsAccount);
    }

    public void depositMoneyIntoSavingsAccount(String idSavingsAccount, double value) {
        this.depositMoneyIntoSavingsAccountApplicationService.run(idSavingsAccount, value);
    }

    public void withdrawMoneyFromSavingsAccount(String idSavingsAccount, double value) {
        this.withdrawMoneyFromSavingsAccountApplicationService.run(idSavingsAccount, value);
    }

    public void transferMoneyToAPocket(String idSavingsAccount, double value) {
        this.transferMoneyToAPocketApplicationService.run(idSavingsAccount, value);
    }

    public double checkSavingAccountBalance(String idSavingAccount) {
        return this.checkSavingsAccountBalanceApplicationService.run(idSavingAccount);
    }

    public List<String> getTransactions(String urlFile) throws IOException {
        return this.getTransactionsApplicationService.run(urlFile);
    }

}
