package co.uniquindio.redesdecomputadores2.infraestructure.server;

import co.uniquindio.redesdecomputadores2.application.service.*;
import co.uniquindio.redesdecomputadores2.domain.model.SavingsAccount;
import co.uniquindio.redesdecomputadores2.domain.service.*;
import co.uniquindio.redesdecomputadores2.infraestructure.adapter.repository.FilesRepositoryInMemory;
import co.uniquindio.redesdecomputadores2.infraestructure.adapter.repository.SavingsAccountRepositoryInMemory;
import co.uniquindio.redesdecomputadores2.infraestructure.controller.SavingsAccountController;

import java.io.IOException;

public class ServerMain {

    public static void main(String[] args) {
        SavingsAccountRepositoryInMemory repository = new SavingsAccountRepositoryInMemory();
        OpenAccountService openAccountService = new OpenAccountService(repository);
        OpenAccountApplicationService openAccountApplicationService = new OpenAccountApplicationService(openAccountService);
        OpenPocketService openPocketService = new OpenPocketService(repository);
        OpenPocketApplicationService openPocketApplicationService = new OpenPocketApplicationService(openPocketService);
        CancelPocketService cancelPocketService = new CancelPocketService(repository);
        CancelPocketApplicationService cancelPocketApplicationService = new CancelPocketApplicationService(cancelPocketService);
        CancelSavingAccountService cancelSavingAccountService = new CancelSavingAccountService(repository);
        CancelSavingsAccountApplicationService cancelSavingsAccountApplicationService = new CancelSavingsAccountApplicationService(cancelSavingAccountService);
        DepositMoneyIntoSavingsAccountService depositMoneyIntoSavingsAccountService = new DepositMoneyIntoSavingsAccountService(repository);
        DepositMoneyIntoSavingsAccountApplicationService depositMoneyIntoSavingsAccountApplicationService = new DepositMoneyIntoSavingsAccountApplicationService(depositMoneyIntoSavingsAccountService);
        WithdrawMoneyFromSavingsAccountService withdrawMoneyFromSavingsAccountService = new WithdrawMoneyFromSavingsAccountService(repository);
        WithdrawMoneyFromSavingsAccountApplicationService withdrawMoneyFromSavingsAccountApplicationService = new WithdrawMoneyFromSavingsAccountApplicationService(withdrawMoneyFromSavingsAccountService);
        TransferMoneyToAPocketService transferMoneyToAPocketService = new TransferMoneyToAPocketService(repository);
        TransferMoneyToAPocketApplicationService transferMoneyToAPocketApplicationService = new TransferMoneyToAPocketApplicationService(transferMoneyToAPocketService);
        CheckSavingsAccountBalanceService checkSavingsAccountBalanceService = new CheckSavingsAccountBalanceService(repository);
        CheckSavingsAccountBalanceApplicationService checkSavingsAccountBalanceApplicationService = new CheckSavingsAccountBalanceApplicationService(checkSavingsAccountBalanceService);
        FilesRepositoryInMemory filesRepository = new FilesRepositoryInMemory();
        GetTransactionsService getTransactionsService = new GetTransactionsService(filesRepository);
        GetTransactionsApplicationService getTransactionsApplicationService = new GetTransactionsApplicationService(getTransactionsService);


        SavingsAccountController controller = new SavingsAccountController(openAccountApplicationService,
                openPocketApplicationService,
                cancelPocketApplicationService,
                cancelSavingsAccountApplicationService,
                depositMoneyIntoSavingsAccountApplicationService,
                withdrawMoneyFromSavingsAccountApplicationService,
                transferMoneyToAPocketApplicationService,
                checkSavingsAccountBalanceApplicationService,
                getTransactionsApplicationService);
        Server server = new Server(controller);
        try {
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*SavingsAccount account = new SavingsAccount("Daniel"
                , "Alejandro", 1200);
        controller.openSavingsAccount(account);
        controller.openPocket("0");
        //controller.cancelPocket("0b");
        //controller.cancelSavingsAccount("0");
        controller.withdrawMoneyFromSavingsAccount("0", 200);
        controller.transferMoneyToAPocket("0", 200);
        System.out.println(controller.checkSavingAccountBalance("0"));*/

    }
}
