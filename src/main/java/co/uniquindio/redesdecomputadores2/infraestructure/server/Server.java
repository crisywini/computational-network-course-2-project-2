package co.uniquindio.redesdecomputadores2.infraestructure.server;

import co.uniquindio.redesdecomputadores2.infraestructure.controller.SavingsAccountController;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static final int PORT = 3400;
    private ServerSocket listener;
    private Socket serverSideSocket;
    private final SavingsAccountController savingsAccountController;

    public Server(SavingsAccountController savingsAccountController) {
        this.savingsAccountController = savingsAccountController;
    }

    public void start() throws IOException {
        listener = new ServerSocket(PORT);
        while (true) {
            serverSideSocket = listener.accept();
            ServerProtocol.doProtocol(serverSideSocket, savingsAccountController);
        }
    }
}
