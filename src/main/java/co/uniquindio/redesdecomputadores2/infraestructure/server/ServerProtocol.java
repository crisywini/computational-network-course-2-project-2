package co.uniquindio.redesdecomputadores2.infraestructure.server;

import co.uniquindio.redesdecomputadores2.domain.exception.*;
import co.uniquindio.redesdecomputadores2.domain.model.SavingsAccount;
import co.uniquindio.redesdecomputadores2.infraestructure.controller.SavingsAccountController;

import co.uniquindio.redesdecomputadores2.domain.model.ConstantServices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ServerProtocol {

    private static final String INSUFFICIENT_INFORMATION_MESSAGE = "Informacion insuficiente para la transaccion!";
    private static PrintWriter toNetwork;
    private static BufferedReader fromNetwork;
    private static final List<String> outs = new ArrayList<>();
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

    public static void doProtocol(Socket socket, SavingsAccountController controller) throws IOException {
        createStreams(socket);
        String message = fromNetwork.readLine();
        String[] messages = message.split(", ");
        String out = "";
        String dateTime = formatter.format(LocalDateTime.now());
        try {
            guaranteeMoreThanOneElement(messages);
            ConstantServices transaction = ConstantServices.valueOf(messages[0]);
            switch (transaction) {
                case ABRIR_CUENTA:
                    String parameter = messages[1];
                    out = openAccount(parameter, controller);
                    break;
                case ABRIR_BOLSILLO:
                    parameter = messages[1];
                    out = openPocket(parameter, controller);
                    break;
                case CANCELAR_BOLSILLO:
                    parameter = messages[1];
                    out = cancelPocket(parameter, controller);
                    break;
                case CANCELAR_CUENTA:
                    parameter = messages[1];
                    out = cancelSavingAccount(parameter, controller);
                    break;
                case DEPOSITAR:
                    parameter = messages[1];
                    String parameter2 = messages[2];
                    out = depositMoneyIntoSavingAccount(parameter, parameter2, controller);
                    break;
                case RETIRAR:
                    parameter = messages[1];
                    parameter2 = messages[2];
                    out = withdrawMoneyFromSavingsAccount(parameter, parameter2, controller);
                    break;
                case TRASLADAR:
                    parameter = messages[1];
                    parameter2 = messages[2];
                    out = transferMoneyToAPocket(parameter, parameter2, controller);
                    break;
                case CONSULTAR:
                    parameter = messages[1];
                    out = checkSavingAccountBalance(parameter, controller);
                    break;
                case CARGAR:
                    parameter = messages[1];
                    out = loadTransactions(parameter, controller);
                    break;
            }
            System.out.println("[Server] " + dateTime + " From Client: " + message);

        } catch (BusinessException e) {
            out = e.getMessage();
        }
        outs.add(dateTime + ", " + message + ", " + out);
        String answer = out;
        System.out.println(outs);
        toNetwork.println(answer);
    }

    private static void createStreams(Socket socket) throws IOException {
        toNetwork = new PrintWriter(socket.getOutputStream(), true);
        fromNetwork = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    private static String openAccount(String parameter, SavingsAccountController controller) {
        String out = "Transaccion Exitosa Cuenta Abierta!";
        try {
            String[] data = parameter.split(" ");
            guaranteeTwoElements(data);
            String userName = data[0];
            String userLastName = data[1];
            SavingsAccount savingsAccount = new SavingsAccount(userName, userLastName, 0);

            controller.openSavingsAccount(savingsAccount);

        } catch (BusinessException | ExistingSavingsAccountException e) {
            out = e.getMessage();
        }

        return out;
    }

    private static String openPocket(String parameter, SavingsAccountController controller) {
        String out = "Transaccion Exitosa Bolsillo Abierto!";
        try {
            guaranteeExistingParameter(parameter);
            controller.openPocket(parameter);
        } catch (BusinessException |
                ExistingPocketException |
                NonExistingSavingsAccountException e) {
            out = e.getMessage();
        }
        return out;
    }

    private static String cancelPocket(String parameter, SavingsAccountController controller) {
        String out = "Transaccion Exitosa Bolsillo Cerrado";

        try {
            guaranteeExistingParameter(parameter);
            controller.cancelPocket(parameter);
        } catch (BusinessException |
                NonExistingSavingsAccountException |
                NonExistingPocketException e) {
            out = e.getMessage();
        }

        return out;
    }

    private static String cancelSavingAccount(String parameter, SavingsAccountController controller) {
        String out = "Transaccion Exitosa Cuenta Cerrada";

        try {
            guaranteeExistingParameter(parameter);
            controller.cancelSavingsAccount(parameter);
        } catch (BusinessException |
                NonExistingSavingsAccountException |
                NonExistingPocketException e) {
            out = e.getMessage();
        }

        return out;
    }

    private static String depositMoneyIntoSavingAccount(String parameter, String parameter2,
                                                        SavingsAccountController controller) {
        String out = "Transaccion Exitosa Deposito Realizado!";

        try {
            guaranteeExistingParameter(parameter);
            guaranteeExistingParameter(parameter2);
            double value = Double.parseDouble(parameter2);
            controller.depositMoneyIntoSavingsAccount(parameter, value);
        } catch (BusinessException |
                NonExistingSavingsAccountException e) {
            out = e.getMessage();
        } catch (NumberFormatException e) {
            out = "El valor debe ser un numero";
        }

        return out;
    }

    private static String withdrawMoneyFromSavingsAccount(String parameter, String parameter2, SavingsAccountController controller) {
        String out = "Transaccion Exitosa Retiro realizado!";

        try {
            guaranteeExistingParameter(parameter);
            guaranteeExistingParameter(parameter2);
            double value = Double.parseDouble(parameter2);
            controller.withdrawMoneyFromSavingsAccount(parameter, value);
        } catch (BusinessException |
                NonExistingSavingsAccountException e) {
            out = e.getMessage();
        } catch (NumberFormatException e) {
            out = "El valor debe ser un numero";
        }

        return out;
    }

    private static String transferMoneyToAPocket(String parameter, String parameter2, SavingsAccountController controller) {
        String out = "Transaccion Exitosa Transferencia Realizada!";

        try {
            guaranteeExistingParameter(parameter);
            guaranteeExistingParameter(parameter2);
            double value = Double.parseDouble(parameter2);
            controller.transferMoneyToAPocket(parameter, value);
        } catch (BusinessException |
                NonExistingSavingsAccountException |
                NonExistingPocketException e) {
            out = e.getMessage();
        } catch (NumberFormatException e) {
            out = "El valor debe ser un numero";
        }

        return out;
    }

    private static String checkSavingAccountBalance(String parameter, SavingsAccountController controller) {
        String out = "Transaccion Exitosa ";

        try {
            guaranteeExistingParameter(parameter);
            double value = controller.checkSavingAccountBalance(parameter);
            out += "Valor de la cuenta es: " + value;
        } catch (BusinessException |
                NonExistingSavingsAccountException e) {
            out = e.getMessage();
        }

        return out;
    }


    private static String loadTransactions(String parameter, SavingsAccountController controller) {
        String out = "Transacciones cargadas!";
        try {
            List<String> transactions = controller.getTransactions(parameter);
            out = doTransactions(transactions, controller);
        } catch (IOException ioe) {
            ioe.printStackTrace();
            out = "Problema leyendo el archivo!";
        }

        return out;
    }

    private static String doTransactions(List<String> transactions, SavingsAccountController controller) {
        String out = "";

        for (String transaction : transactions) {
            String[] messages = transaction.split(", ");
            String dateTime = formatter.format(LocalDateTime.now());
            try {
                guaranteeMoreThanOneElement(messages);
                ConstantServices transactionType = ConstantServices.valueOf(messages[0]);
                switch (transactionType) {
                    case ABRIR_CUENTA:
                        String parameter = messages[1];
                        out = openAccount(parameter, controller);
                        break;
                    case ABRIR_BOLSILLO:
                        parameter = messages[1];
                        out = openPocket(parameter, controller);
                        break;
                    case CANCELAR_BOLSILLO:
                        parameter = messages[1];
                        out = cancelPocket(parameter, controller);
                        break;
                    case CANCELAR_CUENTA:
                        parameter = messages[1];
                        out = cancelSavingAccount(parameter, controller);
                        break;
                    case DEPOSITAR:
                        parameter = messages[1];
                        String parameter2 = messages[2];
                        out = depositMoneyIntoSavingAccount(parameter, parameter2, controller);
                        break;
                    case RETIRAR:
                        parameter = messages[1];
                        parameter2 = messages[2];
                        out = withdrawMoneyFromSavingsAccount(parameter, parameter2, controller);
                        break;
                    case TRASLADAR:
                        parameter = messages[1];
                        parameter2 = messages[2];
                        out = transferMoneyToAPocket(parameter, parameter2, controller);
                        break;
                    case CONSULTAR:
                        parameter = messages[1];
                        out = checkSavingAccountBalance(parameter, controller);
                        break;
                }
            } catch (BusinessException e) {
                out = e.getMessage();
            }
            outs.add(dateTime + ", " + transaction + ", " + out);

        }
        out = "Transacciones realizadas correctamente!";
        return out;
    }


    private static void guaranteeTwoElements(String[] data) {
        if (data.length != 2) {
            throw new BusinessException(INSUFFICIENT_INFORMATION_MESSAGE);
        }
    }

    private static void guaranteeMoreThanOneElement(String[] data) {
        if (data.length < 2) {
            throw new BusinessException(INSUFFICIENT_INFORMATION_MESSAGE);
        }
    }

    private static void guaranteeExistingParameter(String parameter) {
        if (parameter == null || parameter.isEmpty()) {
            throw new BusinessException(INSUFFICIENT_INFORMATION_MESSAGE);
        }
    }

}
