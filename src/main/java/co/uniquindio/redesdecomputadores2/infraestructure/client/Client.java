package co.uniquindio.redesdecomputadores2.infraestructure.client;

import java.io.IOException;
import java.net.Socket;

public class Client {
    public static final int PORT = 3400;
    public static final String SERVER = "localhost";

    private Socket clientSideSocket;

    public void start() throws IOException {
        initSocket();

        ClientProtocol.doProtocol(clientSideSocket);

        closeSocket();
    }

    public void initSocket() throws IOException {
        clientSideSocket = new Socket(SERVER, PORT);
    }

    public void closeSocket() throws IOException {
        clientSideSocket.close();
    }

    public String openSavingsAccount(String userName) throws IOException {
        initSocket();
        String result = ClientProtocol.doOpenAccountProtocol(clientSideSocket, userName);
        closeSocket();
        return result;
    }

    public String openPocket(String accountNumber) throws IOException {
        initSocket();
        String result = ClientProtocol.doOpenPocketProtocol(clientSideSocket, accountNumber);
        closeSocket();
        return result;
    }

    public String cancelSavingsAccount(String accountNumber) throws IOException {
        initSocket();
        String result = ClientProtocol.doCancelSavingsAccountProtocol(clientSideSocket, accountNumber);
        closeSocket();
        return result;
    }

    public String cancelPocket(String pocketNumber) throws IOException {
        initSocket();
        String result = ClientProtocol.doCancelPocketProtocol(clientSideSocket, pocketNumber);
        closeSocket();
        return result;
    }

    public String depositar(String pocketNumber, String quantity) throws IOException {
        initSocket();
        String result = ClientProtocol.doDepositProtocol(clientSideSocket, pocketNumber, quantity);
        closeSocket();
        return result;
    }

    public String withdrawMoney(String pocketNumber, String quantity) throws IOException {
        initSocket();
        String result = ClientProtocol.doWithdrawProtocol(clientSideSocket, pocketNumber, quantity);
        closeSocket();
        return result;
    }

    public String transferMoney(String pocketNumber, String quantity) throws IOException {
        initSocket();
        String result = ClientProtocol.doTransferProtocol(clientSideSocket, pocketNumber, quantity);
        closeSocket();
        return result;
    }

    public String checkBalance(String accountPocketNumber) throws IOException {
        initSocket();
        String result = ClientProtocol.doCheckProtocol(clientSideSocket, accountPocketNumber);
        closeSocket();
        return result;
    }
}
