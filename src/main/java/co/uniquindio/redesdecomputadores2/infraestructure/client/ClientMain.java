package co.uniquindio.redesdecomputadores2.infraestructure.client;

import java.io.IOException;

public class ClientMain {
    public static void main(String[] args) {
        Client client = new Client();
        try {
            client.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
