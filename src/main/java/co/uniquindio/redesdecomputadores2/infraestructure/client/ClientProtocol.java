package co.uniquindio.redesdecomputadores2.infraestructure.client;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ClientProtocol {
    private static final Scanner SCANNER = new Scanner(System.in);

    private static PrintWriter toNetwork;
    private static BufferedReader fromNetwork;

    public static void doProtocol(Socket socket) throws IOException {
        createStreams(socket);

        System.out.println("Seleccionar Transaccion: \n" +
                "1) Abrir Cuenta \n" +
                "2) Crear Bolsillo \n" +
                "3) Cancelar Bolsillo \n" +
                "4) Cancelar Cuenta de Ahorros \n" +
                "5) Depositar Dinero en una Cuenta \n" +
                "6) Retirar Dinero de una Cuenta \n" +
                "7) Trasladar a un Bolsillo \n" +
                "8) Consultar Saldo \n" +
                "9)  Cargar Transacciones");
        int option = Integer.parseInt(SCANNER.nextLine());
        String fromUser = "";
        switch (option) {
            case 1:
                String complement = SCANNER.nextLine();
                fromUser = "ABRIR_CUENTA, " + complement;
                break;
            case 2:
                complement = SCANNER.nextLine();
                fromUser = "ABRIR_BOLSILLO, " + complement;
                break;
            case 3:
                complement = SCANNER.nextLine();
                fromUser = "CANCELAR_BOLSILLO, " + complement;
                break;
            case 4:
                complement = SCANNER.nextLine();
                fromUser = "CANCELAR_CUENTA, " + complement;
                break;
            case 5:
                complement = SCANNER.nextLine();
                fromUser = "DEPOSITAR, " + complement;
                break;
            case 6:
                complement = SCANNER.nextLine();
                fromUser = "RETIRAR, " + complement;
                break;
            case 7:
                complement = SCANNER.nextLine();
                fromUser = "TRASLADAR, " + complement;
                break;
            case 8:
                complement = SCANNER.nextLine();
                fromUser = "CONSULTAR, " + complement;
                break;
            case 9:
                complement = SCANNER.nextLine();
                fromUser = "CARGAR, " + complement;
                break;
        }

        toNetwork.println(fromUser);

        String fromServer = fromNetwork.readLine();
        System.out.println("[Client] From server: " + fromServer);
    }

    private static void createStreams(Socket socket) throws IOException {
        toNetwork = new PrintWriter(socket.getOutputStream(), true);
        fromNetwork = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    public static String doOpenAccountProtocol(Socket socket, String userName) throws IOException {
        createStreams(socket);

        String fromUser = "ABRIR_CUENTA, " + userName;

        toNetwork.println(fromUser);

        String fromServer = fromNetwork.readLine();
        return fromServer;
    }

    public static String doOpenPocketProtocol(Socket socket, String accountNumber) throws IOException {
        createStreams(socket);

        String fromUser = "ABRIR_BOLSILLO, " + accountNumber;

        toNetwork.println(fromUser);

        String fromServer = fromNetwork.readLine();
        return fromServer;
    }

    public static String doCancelSavingsAccountProtocol(Socket socket, String accountNumber) throws IOException {
        createStreams(socket);

        String fromUser = "CANCELAR_CUENTA, " + accountNumber;

        toNetwork.println(fromUser);

        String fromServer = fromNetwork.readLine();
        return fromServer;
    }

    public static String doCancelPocketProtocol(Socket socket, String pocketNumber) throws IOException {
        createStreams(socket);

        String fromUser = "CANCELAR_BOLSILLO, " + pocketNumber;

        toNetwork.println(fromUser);

        String fromServer = fromNetwork.readLine();
        return fromServer;
    }

    public static String doDepositProtocol(Socket socket, String accountNumber, String quantity) throws IOException {
        createStreams(socket);

        String fromUser = "DEPOSITAR, " + accountNumber + ", " + quantity;

        toNetwork.println(fromUser);

        String fromServer = fromNetwork.readLine();
        return fromServer;
    }

    public static String doWithdrawProtocol(Socket socket, String accountNumber, String quantity) throws IOException {
        createStreams(socket);

        String fromUser = "RETIRAR, " + accountNumber + ", " + quantity;

        toNetwork.println(fromUser);

        String fromServer = fromNetwork.readLine();
        return fromServer;
    }

    public static String doTransferProtocol(Socket socket, String accountNumber, String quantity) throws IOException {
        createStreams(socket);

        String fromUser = "TRASLADAR, " + accountNumber + ", " + quantity;

        toNetwork.println(fromUser);

        String fromServer = fromNetwork.readLine();
        return fromServer;
    }

    public static String doCheckProtocol(Socket socket, String accountPocketNumber) throws IOException {
        createStreams(socket);

        String fromUser = "CONSULTAR, " + accountPocketNumber;

        toNetwork.println(fromUser);

        String fromServer = fromNetwork.readLine();
        return fromServer;
    }

}
