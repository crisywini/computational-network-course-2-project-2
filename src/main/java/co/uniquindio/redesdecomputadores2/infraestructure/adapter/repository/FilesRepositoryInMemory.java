package co.uniquindio.redesdecomputadores2.infraestructure.adapter.repository;

import co.uniquindio.redesdecomputadores2.domain.port.repository.FilesRepository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FilesRepositoryInMemory implements FilesRepository {

    @Override
    public List<String> getTransactions(String urlFile) throws IOException {
        List<String> transactions = new ArrayList<>();
        FileReader fileReader = new FileReader(urlFile);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = "";
        while ((line = bufferedReader.readLine()) != null) {
            transactions.add(line );
        }
        bufferedReader.close();
        fileReader.close();
        return transactions;
    }
}
