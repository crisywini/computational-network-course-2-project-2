package co.uniquindio.redesdecomputadores2.infraestructure.adapter.repository;

import co.uniquindio.redesdecomputadores2.domain.model.SavingsAccount;
import co.uniquindio.redesdecomputadores2.domain.port.repository.SavingsAccountRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class SavingsAccountRepositoryInMemory implements SavingsAccountRepository {

    private AtomicInteger atomicInteger = new AtomicInteger();
    private Map<Integer, SavingsAccount> savingsAccountMap = new HashMap<>();


    @Override
    public void openAccount(SavingsAccount account) {

        int id = atomicInteger.getAndIncrement();
        account.setId(id + "");
        savingsAccountMap.put(id,
                account);
    }

    @Override
    public void openPocket(String idSavingsAccount) {

        int id = Integer.parseInt(idSavingsAccount);
        SavingsAccount mainAccount = savingsAccountMap.get(id);

        SavingsAccount pocket = new SavingsAccount(mainAccount.getId() + "b",
                mainAccount.getUserName(), mainAccount.getUserLastName(), 0);
        mainAccount.setPocket(pocket);
    }

    @Override
    public void cancelPocket(String idPocket) {
        String idMainAccount = idPocket.replace("b", "");
        int id = Integer.parseInt(idMainAccount);
        SavingsAccount savingsAccount = savingsAccountMap.get(id);
        double balancePocket = savingsAccount.getPocket().getBalance();
        savingsAccount.setPocket(null);
        savingsAccount.setBalance(savingsAccount.getBalance() + balancePocket);
    }

    @Override
    public void cancelSavingAccount(String idSavingAccount) {
        int id = Integer.parseInt(idSavingAccount);
        savingsAccountMap.remove(id);
    }

    @Override
    public void depositMoneyIntoSavingAccount(String idSavingAccount, double value) {

        int id = Integer.parseInt(idSavingAccount);
        SavingsAccount savingsAccount = savingsAccountMap.get(id);
        savingsAccount.setBalance(savingsAccount.getBalance() + value);
    }

    @Override
    public void withdrawMoneyFromSavingsAccount(String idSavingAccount, double value) {
        int id = Integer.parseInt(idSavingAccount);
        SavingsAccount savingsAccount = savingsAccountMap.get(id);
        savingsAccount.setBalance(savingsAccount.getBalance() - value);
    }

    @Override
    public void transferMoneyToAPocket(String idSavingAccount, double value) {
        int id = Integer.parseInt(idSavingAccount);
        SavingsAccount savingsAccount = savingsAccountMap.get(id);
        SavingsAccount pocket = savingsAccount.getPocket();
        pocket.setBalance(value);
        savingsAccount.setBalance(savingsAccount.getBalance() - value);
    }

    @Override
    public double checkSavingAccountBalance(String idSavingAccount) {
        int id = Integer.parseInt(idSavingAccount);
        SavingsAccount savingsAccount = savingsAccountMap.get(id);
        return savingsAccount.getBalance();
    }

    @Override
    public SavingsAccount getSavingAccount(String idSavingAccount) {
        int id = Integer.parseInt(idSavingAccount);
        return savingsAccountMap.get(id);
    }

    @Override
    public boolean existsSavingAccount(String userName, String userLastName) {

        return savingsAccountMap.values()
                .stream()
                .filter(savingsAccount ->
                        (savingsAccount.getUserName().equals(userName)
                                && savingsAccount.getUserLastName().equals(userLastName)))
                .findFirst()
                .isPresent();
    }

    @Override
    public boolean existsPocket(SavingsAccount mainAccount) {
        return mainAccount.getPocket() != null;
    }

    @Override
    public boolean existsSavingsAccount(String idSavingsAccount) {
        int id = Integer.parseInt(idSavingsAccount);
        return savingsAccountMap.get(id) != null;
    }

}
