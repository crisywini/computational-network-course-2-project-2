package co.uniquindio.redesdecomputadores2.gui.controller;

import co.uniquindio.redesdecomputadores2.infraestructure.client.Client;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

import java.io.IOException;

public class MainController {

    private Client client;

    @FXML
    public void initialize() {
        client = new Client();
    }

    @FXML
    private TextField crearCuentaNombretxt;

    @FXML
    private TextField crearCuentaApellidotxt;

    @FXML
    private TextField cancelarCuentatxt;

    @FXML
    private TextField bolsillotxt;

    @FXML
    private TextField gestionarCuentaNumerotxt;

    @FXML
    private TextField gestionarCeuntaValortxt;

    @FXML
    private TextField consultartxt;

    @FXML
    void crearCuenta() {
        try {
            String result = client.openSavingsAccount(crearCuentaNombretxt.getText() + " " + crearCuentaApellidotxt.getText());
            imprimirInfo(result);
        } catch (IOException e) {
            imprimirError(e);
        }
    }

    @FXML
    void crearBolsillo() {
        try {
            String result = client.openPocket(bolsillotxt.getText());
            imprimirInfo(result);
        } catch (IOException e) {
            imprimirError(e);
        }
    }

    @FXML
    void cancelarCuenta() {
        try {
            String result = client.cancelSavingsAccount(cancelarCuentatxt.getText());
            imprimirInfo(result);
        } catch (IOException e) {
            imprimirError(e);
        }
    }

    @FXML
    void cancelarBolsillo() {
        try {
            String result = client.cancelPocket(bolsillotxt.getText());
            imprimirInfo(result);
        } catch (IOException e) {
            imprimirError(e);
        }
    }

    @FXML
    void depositar() {
        try {
            String result = client.depositar(gestionarCuentaNumerotxt.getText(), gestionarCeuntaValortxt.getText());
            imprimirInfo(result);
        } catch (IOException e) {
            imprimirError(e);
        }
    }

    @FXML
    void retirar() {
        try {
            String result = client.withdrawMoney(gestionarCuentaNumerotxt.getText(), gestionarCeuntaValortxt.getText());
            imprimirInfo(result);
        } catch (IOException e) {
            imprimirError(e);
        }
    }

    @FXML
    void trasladar() {
        try {
            String result = client.transferMoney(gestionarCuentaNumerotxt.getText(), gestionarCeuntaValortxt.getText());
            imprimirInfo(result);
        } catch (IOException e) {
            imprimirError(e);
        }
    }

    @FXML
    void consultar() {
        try {
            String result = client.checkBalance(gestionarCuentaNumerotxt.getText());
            imprimirInfo(result);
        } catch (IOException e) {
            imprimirError(e);
        }
    }

    private void imprimirError(IOException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR, e.getMessage());
        alert.setHeaderText("");
        alert.show();
    }

    private void imprimirInfo(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, message);
        alert.setTitle("Informacion");
        alert.setHeaderText("");
        alert.show();
    }
}

