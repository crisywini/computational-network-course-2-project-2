package co.uniquindio.redesdecomputadores2.application.service;

import co.uniquindio.redesdecomputadores2.domain.service.TransferMoneyToAPocketService;

public class TransferMoneyToAPocketApplicationService {
    private final TransferMoneyToAPocketService transferMoneyToAPocketService;

    public TransferMoneyToAPocketApplicationService(TransferMoneyToAPocketService transferMoneyToAPocketService) {
        this.transferMoneyToAPocketService = transferMoneyToAPocketService;
    }

    public void run(String idSavingAccount, double value) {
        this.transferMoneyToAPocketService.transferMoney(idSavingAccount, value);
    }
}
