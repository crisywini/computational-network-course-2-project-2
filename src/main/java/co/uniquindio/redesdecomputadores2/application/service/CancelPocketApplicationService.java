package co.uniquindio.redesdecomputadores2.application.service;

import co.uniquindio.redesdecomputadores2.domain.service.CancelPocketService;

public class CancelPocketApplicationService {

    private final CancelPocketService cancelPocketService;

    public CancelPocketApplicationService(CancelPocketService cancelPocketService) {
        this.cancelPocketService = cancelPocketService;
    }
    public void run(String idPocket){
        this.cancelPocketService.cancelPocket(idPocket);
    }
}
