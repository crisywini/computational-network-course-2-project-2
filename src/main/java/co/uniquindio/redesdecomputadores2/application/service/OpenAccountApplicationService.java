package co.uniquindio.redesdecomputadores2.application.service;

import co.uniquindio.redesdecomputadores2.domain.exception.ExistingSavingsAccountException;
import co.uniquindio.redesdecomputadores2.domain.model.SavingsAccount;
import co.uniquindio.redesdecomputadores2.domain.service.OpenAccountService;

public class OpenAccountApplicationService {
    private final OpenAccountService openAccountService;

    public OpenAccountApplicationService(OpenAccountService openAccountService) {
        this.openAccountService = openAccountService;
    }

    public void run(SavingsAccount savingsAccount) {
        openAccountService.open(savingsAccount);
    }
}
