package co.uniquindio.redesdecomputadores2.application.service;

import co.uniquindio.redesdecomputadores2.domain.service.DepositMoneyIntoSavingsAccountService;

public class DepositMoneyIntoSavingsAccountApplicationService {
    private final DepositMoneyIntoSavingsAccountService depositMoneyIntoSavingsAccountService;

    public DepositMoneyIntoSavingsAccountApplicationService(DepositMoneyIntoSavingsAccountService depositMoneyIntoSavingsAccountService) {
        this.depositMoneyIntoSavingsAccountService = depositMoneyIntoSavingsAccountService;
    }

    public void run(String idSavingAccount, double value) {
        this.depositMoneyIntoSavingsAccountService.deposit(idSavingAccount, value);
    }
}
