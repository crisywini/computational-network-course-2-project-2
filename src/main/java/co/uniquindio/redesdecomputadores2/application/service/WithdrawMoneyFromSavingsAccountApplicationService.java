package co.uniquindio.redesdecomputadores2.application.service;

import co.uniquindio.redesdecomputadores2.domain.service.WithdrawMoneyFromSavingsAccountService;

public class WithdrawMoneyFromSavingsAccountApplicationService {
    private final WithdrawMoneyFromSavingsAccountService withdrawMoneyFromSavingsAccountService;

    public WithdrawMoneyFromSavingsAccountApplicationService(WithdrawMoneyFromSavingsAccountService withdrawMoneyFromSavingsAccountService) {
        this.withdrawMoneyFromSavingsAccountService = withdrawMoneyFromSavingsAccountService;
    }

    public void run(String idSavingsAccount, double value) {
        this.withdrawMoneyFromSavingsAccountService.withdrawMoney(idSavingsAccount, value);
    }
}

