package co.uniquindio.redesdecomputadores2.application.service;

import co.uniquindio.redesdecomputadores2.domain.service.CancelSavingAccountService;

public class CancelSavingsAccountApplicationService {

    private final CancelSavingAccountService cancelSavingAccountService;

    public CancelSavingsAccountApplicationService(CancelSavingAccountService cancelSavingAccountService) {
        this.cancelSavingAccountService = cancelSavingAccountService;
    }

    public void run(String idApplicationService) {
        this.cancelSavingAccountService.cancelSavingsAccount(idApplicationService);
    }
}
