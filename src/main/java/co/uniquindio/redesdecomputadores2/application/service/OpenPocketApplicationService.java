package co.uniquindio.redesdecomputadores2.application.service;

import co.uniquindio.redesdecomputadores2.domain.model.SavingsAccount;
import co.uniquindio.redesdecomputadores2.domain.service.OpenPocketService;

public class OpenPocketApplicationService {
    private final OpenPocketService openPocketService;

    public OpenPocketApplicationService(OpenPocketService openPocketService) {
        this.openPocketService = openPocketService;
    }

    public void run(String idSavingsAccount) {
        openPocketService.open(idSavingsAccount);
    }
}
