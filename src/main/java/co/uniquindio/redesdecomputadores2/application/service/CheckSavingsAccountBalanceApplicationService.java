package co.uniquindio.redesdecomputadores2.application.service;

import co.uniquindio.redesdecomputadores2.domain.service.CheckSavingsAccountBalanceService;

public class CheckSavingsAccountBalanceApplicationService {
    private final CheckSavingsAccountBalanceService checkSavingsAccountBalanceService;

    public CheckSavingsAccountBalanceApplicationService(CheckSavingsAccountBalanceService checkSavingsAccountBalanceService) {
        this.checkSavingsAccountBalanceService = checkSavingsAccountBalanceService;
    }

    public double run(String idSavingsAccount) {
        return checkSavingsAccountBalanceService.checkBalance(idSavingsAccount);
    }
}
