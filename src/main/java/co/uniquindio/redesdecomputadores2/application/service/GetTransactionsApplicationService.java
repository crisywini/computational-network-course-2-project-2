package co.uniquindio.redesdecomputadores2.application.service;

import co.uniquindio.redesdecomputadores2.domain.service.GetTransactionsService;

import java.io.IOException;
import java.util.List;

public class GetTransactionsApplicationService {
    private final GetTransactionsService getTransactionsService;

    public GetTransactionsApplicationService(GetTransactionsService getTransactionsService) {
        this.getTransactionsService = getTransactionsService;
    }

    public List<String> run(String urlFile) throws IOException {
        return getTransactionsService.getTransactions(urlFile);
    }
}
