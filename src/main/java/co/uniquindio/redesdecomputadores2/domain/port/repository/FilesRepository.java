package co.uniquindio.redesdecomputadores2.domain.port.repository;

import java.io.IOException;
import java.util.List;

public interface FilesRepository {
    List<String> getTransactions(String urlFile)throws IOException;
}
