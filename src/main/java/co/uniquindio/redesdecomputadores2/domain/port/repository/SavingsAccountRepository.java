package co.uniquindio.redesdecomputadores2.domain.port.repository;

import co.uniquindio.redesdecomputadores2.domain.exception.ExistingPocketException;
import co.uniquindio.redesdecomputadores2.domain.exception.ExistingSavingsAccountException;
import co.uniquindio.redesdecomputadores2.domain.model.SavingsAccount;

public interface SavingsAccountRepository {

    void openAccount(SavingsAccount account);

    void openPocket(String idSavingsAccount);

    void cancelPocket(String idPocket);

    void cancelSavingAccount(String idSavingAccount);

    void depositMoneyIntoSavingAccount(String idSavingAccount, double value);

    void withdrawMoneyFromSavingsAccount(String idSavingAccount, double value);

    void transferMoneyToAPocket(String idSavingAccount, double value);

    double checkSavingAccountBalance(String idSavingAccount);

    SavingsAccount getSavingAccount(String idSavingAccount);

    boolean existsSavingAccount(String userName, String userLastName);

    boolean existsPocket(SavingsAccount mainAccount);

    boolean existsSavingsAccount(String idSavingsAccount);


}
