package co.uniquindio.redesdecomputadores2.domain.service;

import co.uniquindio.redesdecomputadores2.domain.port.repository.FilesRepository;

import java.io.IOException;
import java.util.List;

public class GetTransactionsService {

    private final FilesRepository filesRepository;

    public GetTransactionsService(FilesRepository filesRepository) {
        this.filesRepository = filesRepository;
    }

    public List<String> getTransactions(String urlFile) throws IOException {
        return filesRepository.getTransactions(urlFile);
    }
}
