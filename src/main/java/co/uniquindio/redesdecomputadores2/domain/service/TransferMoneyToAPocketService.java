package co.uniquindio.redesdecomputadores2.domain.service;

import co.uniquindio.redesdecomputadores2.domain.exception.BusinessException;
import co.uniquindio.redesdecomputadores2.domain.exception.NonExistingPocketException;
import co.uniquindio.redesdecomputadores2.domain.exception.NonExistingSavingsAccountException;
import co.uniquindio.redesdecomputadores2.domain.model.SavingsAccount;
import co.uniquindio.redesdecomputadores2.domain.port.repository.SavingsAccountRepository;

public class TransferMoneyToAPocketService {

    private static final String INCONSISTENCY_VALUE_MESSAGE = "El saldo a transferir es inconsistente";
    private static final String INSUFFICIENT_BALANCE_MESSAGE = "Saldo insuficiente para transferir";
    private static final String NON_EXISTING_SAVINGS_ACCOUNT_MESSAGE = "La cuenta no existe, no se puede transferir!";
    private static final String NON_EXISTING_POCKET_MESSAGE = "La cuenta no tiene un bolsillo, no se puede transferir!";

    private final SavingsAccountRepository savingsAccountRepository;

    public TransferMoneyToAPocketService(SavingsAccountRepository savingsAccountRepository) {
        this.savingsAccountRepository = savingsAccountRepository;
    }

    public void transferMoney(String idSavingAccount, double value) {
        guaranteeExistingSavingsAccount(idSavingAccount);
        SavingsAccount savingsAccount = savingsAccountRepository.getSavingAccount(idSavingAccount);
        guaranteeExistingPocket(savingsAccount);
        guaranteeNoBalanceInconsistency(value);
        guaranteeInsufficientBalance(idSavingAccount, value);
        savingsAccountRepository.transferMoneyToAPocket(idSavingAccount, value);
    }

    private void guaranteeExistingSavingsAccount(String idSavingsAccount) {
        if (!savingsAccountRepository.existsSavingsAccount(idSavingsAccount)) {
            throw new NonExistingSavingsAccountException(NON_EXISTING_SAVINGS_ACCOUNT_MESSAGE);
        }
    }

    private void guaranteeExistingPocket(SavingsAccount savingsAccount) {
        if (!savingsAccountRepository.existsPocket(savingsAccount)) {
            throw new NonExistingPocketException(NON_EXISTING_POCKET_MESSAGE);
        }
    }

    private void guaranteeNoBalanceInconsistency(double value) {
        if (value <= 0) {
            throw new BusinessException(INCONSISTENCY_VALUE_MESSAGE);
        }
    }

    private void guaranteeInsufficientBalance(String idSavingsAccount, double value) {
        if (savingsAccountRepository.checkSavingAccountBalance(idSavingsAccount) < value) {
            throw new BusinessException(INSUFFICIENT_BALANCE_MESSAGE);
        }
    }
}
