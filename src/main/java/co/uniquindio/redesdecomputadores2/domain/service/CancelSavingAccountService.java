package co.uniquindio.redesdecomputadores2.domain.service;

import co.uniquindio.redesdecomputadores2.domain.exception.BusinessException;
import co.uniquindio.redesdecomputadores2.domain.exception.NonExistingSavingsAccountException;
import co.uniquindio.redesdecomputadores2.domain.model.SavingsAccount;
import co.uniquindio.redesdecomputadores2.domain.port.repository.SavingsAccountRepository;

public class CancelSavingAccountService {

    private static final String NON_ZERO_BALANCE_EXCEPTION = "La cuenta no tiene saldo cero, no se puede eliminar!";
    private static final String EXIST_POCKET_MESSAGE = "La cuenta tiene un bolsillo asociado, no se puede eliminar!";
    private static final String NON_EXISTING_SAVINGS_ACCOUNT_MESSAGE = "La cuenta no existe, no se puede eliminar!";

    private final SavingsAccountRepository savingsAccountRepository;

    public CancelSavingAccountService(SavingsAccountRepository savingsAccountRepository) {
        this.savingsAccountRepository = savingsAccountRepository;
    }

    public void cancelSavingsAccount(String idSavingsAccount) {
        guaranteeExistingSavingsAccount(idSavingsAccount);
        SavingsAccount account = savingsAccountRepository.getSavingAccount(idSavingsAccount);
        guaranteeNonExistingPocket(account);
        guaranteeZeroBalance(idSavingsAccount);
        savingsAccountRepository.cancelSavingAccount(idSavingsAccount);
    }

    private void guaranteeExistingSavingsAccount(String idSavingsAccount) {
        if (!savingsAccountRepository.existsSavingsAccount(idSavingsAccount)) {
            throw new NonExistingSavingsAccountException(NON_EXISTING_SAVINGS_ACCOUNT_MESSAGE);
        }
    }

    private void guaranteeNonExistingPocket(SavingsAccount savingsAccount) {
        if (savingsAccountRepository.existsPocket(savingsAccount)) {
            throw new BusinessException(EXIST_POCKET_MESSAGE);
        }
    }

    private void guaranteeZeroBalance(String idSavingsAccount) {
        if (savingsAccountRepository.checkSavingAccountBalance(idSavingsAccount) != 0) {
            throw new BusinessException(NON_ZERO_BALANCE_EXCEPTION);
        }
    }
}
