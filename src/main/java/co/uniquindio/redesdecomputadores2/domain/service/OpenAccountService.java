package co.uniquindio.redesdecomputadores2.domain.service;

import co.uniquindio.redesdecomputadores2.domain.exception.ExistingSavingsAccountException;
import co.uniquindio.redesdecomputadores2.domain.model.SavingsAccount;
import co.uniquindio.redesdecomputadores2.domain.port.repository.SavingsAccountRepository;

public class OpenAccountService {

    private static final String EXISTING_ACCOUNT_MESSAGE = "La cuenta ya existe!";

    private final SavingsAccountRepository savingsAccountRepository;

    public OpenAccountService(SavingsAccountRepository savingsAccountRepository) {
        this.savingsAccountRepository = savingsAccountRepository;
    }

    public void open(SavingsAccount savingsAccount){
        guaranteeNonRepeatingSavingsAccount(savingsAccount.getUserName(), savingsAccount.getUserLastName());
        savingsAccountRepository.openAccount(savingsAccount);
    }

    private void guaranteeNonRepeatingSavingsAccount(String userName, String userLastname){
        if (this.savingsAccountRepository.existsSavingAccount(userName, userLastname)) {
            throw new ExistingSavingsAccountException(EXISTING_ACCOUNT_MESSAGE);
        }
    }


}
