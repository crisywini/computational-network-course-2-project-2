package co.uniquindio.redesdecomputadores2.domain.service;

import co.uniquindio.redesdecomputadores2.domain.exception.NonExistingSavingsAccountException;
import co.uniquindio.redesdecomputadores2.domain.port.repository.SavingsAccountRepository;

public class CheckSavingsAccountBalanceService {
    private static final String NON_EXISTING_SAVINGS_ACCOUNT_MESSAGE = "La cuenta no existe, no se puede consultar el saldo!";

    private final SavingsAccountRepository savingsAccountRepository;

    public CheckSavingsAccountBalanceService(SavingsAccountRepository savingsAccountRepository) {
        this.savingsAccountRepository = savingsAccountRepository;
    }

    public double checkBalance(String idSavingsAccount) {
        guaranteeExistingSavingsAccount(idSavingsAccount);
        return savingsAccountRepository.checkSavingAccountBalance(idSavingsAccount);
    }

    private void guaranteeExistingSavingsAccount(String idSavingsAccount) {
        if (!savingsAccountRepository.existsSavingsAccount(idSavingsAccount)) {
            throw new NonExistingSavingsAccountException(NON_EXISTING_SAVINGS_ACCOUNT_MESSAGE);
        }
    }
}
