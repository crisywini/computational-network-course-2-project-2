package co.uniquindio.redesdecomputadores2.domain.service;

import co.uniquindio.redesdecomputadores2.domain.exception.BusinessException;
import co.uniquindio.redesdecomputadores2.domain.exception.NonExistingSavingsAccountException;
import co.uniquindio.redesdecomputadores2.domain.port.repository.SavingsAccountRepository;

public class WithdrawMoneyFromSavingsAccountService {
    private static final String INCONSISTENCY_VALUE_MESSAGE = "El saldo a ingresar es inconsistente";
    private static final String INSUFFICIENT_BALANCE_MESSAGE = "Saldo insuficiente para retirar";
    private static final String NON_EXISTING_SAVINGS_ACCOUNT_MESSAGE = "La cuenta no existe, no se puede retirar!";

    private final SavingsAccountRepository savingsAccountRepository;

    public WithdrawMoneyFromSavingsAccountService(SavingsAccountRepository savingsAccountRepository) {
        this.savingsAccountRepository = savingsAccountRepository;
    }


    public void withdrawMoney(String idSavingsAccount, double value) {
        guaranteeExistingSavingsAccount(idSavingsAccount);
        guaranteeNoBalanceInconsistency(value);
        guaranteeInsufficientBalance(idSavingsAccount, value);
        savingsAccountRepository.withdrawMoneyFromSavingsAccount(idSavingsAccount, value);
    }

    private void guaranteeExistingSavingsAccount(String idSavingsAccount) {
        if (!savingsAccountRepository.existsSavingsAccount(idSavingsAccount)) {
            throw new NonExistingSavingsAccountException(NON_EXISTING_SAVINGS_ACCOUNT_MESSAGE);
        }
    }

    private void guaranteeNoBalanceInconsistency(double value) {
        if (value <= 0) {
            throw new BusinessException(INCONSISTENCY_VALUE_MESSAGE);
        }
    }

    private void guaranteeInsufficientBalance(String idSavingsAccount, double value) {
        if (savingsAccountRepository.checkSavingAccountBalance(idSavingsAccount) < value) {
            throw new BusinessException(INSUFFICIENT_BALANCE_MESSAGE);
        }
    }
}
