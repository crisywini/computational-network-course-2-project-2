package co.uniquindio.redesdecomputadores2.domain.service;

import co.uniquindio.redesdecomputadores2.domain.exception.NonExistingPocketException;
import co.uniquindio.redesdecomputadores2.domain.exception.NonExistingSavingsAccountException;
import co.uniquindio.redesdecomputadores2.domain.model.SavingsAccount;
import co.uniquindio.redesdecomputadores2.domain.port.repository.SavingsAccountRepository;

public class CancelPocketService {

    private static final String NON_EXISTING_SAVINGS_ACCOUNT_MESSAGE = "La cuenta principal no existe!";
    private static final String NON_EXISTING_POCKET_MESSAGE = "El bolsillo no existe!";
    private final SavingsAccountRepository savingsAccountRepository;


    public CancelPocketService(SavingsAccountRepository savingsAccountRepository) {
        this.savingsAccountRepository = savingsAccountRepository;
    }

    public void cancelPocket(String idPocket) {
        String idSavingsAccount = idPocket.replace("b", "");
        guaranteeExistingSavingsAccount(idSavingsAccount);
        SavingsAccount savingsAccount = savingsAccountRepository.getSavingAccount(idSavingsAccount);
        guaranteeExistingPocket(savingsAccount);
        savingsAccountRepository.cancelPocket(idPocket);
    }

    private void guaranteeExistingPocket(SavingsAccount savingsAccount) {
        if (!savingsAccountRepository.existsPocket(savingsAccount)) {
            throw new NonExistingPocketException(NON_EXISTING_POCKET_MESSAGE);
        }

    }

    private void guaranteeExistingSavingsAccount(String idSavingsAccount) {
        if (!savingsAccountRepository.existsSavingsAccount(idSavingsAccount)) {
            throw new NonExistingSavingsAccountException(NON_EXISTING_SAVINGS_ACCOUNT_MESSAGE);
        }
    }

}
