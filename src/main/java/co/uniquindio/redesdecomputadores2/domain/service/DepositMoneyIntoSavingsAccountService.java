package co.uniquindio.redesdecomputadores2.domain.service;

import co.uniquindio.redesdecomputadores2.domain.exception.BusinessException;
import co.uniquindio.redesdecomputadores2.domain.exception.NonExistingSavingsAccountException;
import co.uniquindio.redesdecomputadores2.domain.port.repository.SavingsAccountRepository;

public class DepositMoneyIntoSavingsAccountService {
    private static final String NON_EXISTING_SAVINGS_ACCOUNT_MESSAGE = "La cuenta no existe, no se puede depositarl!";
    private static final String INCONSISTENCY_VALUE_MESSAGE = "El saldo a ingresar es inconsistente";

    private final SavingsAccountRepository savingsAccountRepository;

    public DepositMoneyIntoSavingsAccountService(SavingsAccountRepository savingsAccountRepository) {
        this.savingsAccountRepository = savingsAccountRepository;
    }

    public void deposit(String idSavingAccount, double value) {
        guaranteeExistingSavingsAccount(idSavingAccount);
        guaranteeNoBalanceInconsistency(value);
        savingsAccountRepository.depositMoneyIntoSavingAccount(idSavingAccount, value);
    }


    private void guaranteeExistingSavingsAccount(String idSavingsAccount) {
        if (!savingsAccountRepository.existsSavingsAccount(idSavingsAccount)) {
            throw new NonExistingSavingsAccountException(NON_EXISTING_SAVINGS_ACCOUNT_MESSAGE);
        }
    }

    private void guaranteeNoBalanceInconsistency(double value) {
        if (value <= 0) {
            throw new BusinessException(INCONSISTENCY_VALUE_MESSAGE);
        }
    }
}
