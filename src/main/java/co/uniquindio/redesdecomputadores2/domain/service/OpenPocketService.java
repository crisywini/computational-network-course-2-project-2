package co.uniquindio.redesdecomputadores2.domain.service;

import co.uniquindio.redesdecomputadores2.domain.exception.ExistingPocketException;
import co.uniquindio.redesdecomputadores2.domain.exception.NonExistingSavingsAccountException;
import co.uniquindio.redesdecomputadores2.domain.model.SavingsAccount;
import co.uniquindio.redesdecomputadores2.domain.port.repository.SavingsAccountRepository;

public class OpenPocketService {

    private static final String EXISTING_POCKET_MESSAGE = "La cuenta no existe!";
    private static final String NON_EXISTING_SAVINGS_ACCOUNT_MESSAGE = "El bolsillo para la cuenta ya existe!";

    private final SavingsAccountRepository savingsAccountRepository;

    public OpenPocketService(SavingsAccountRepository savingsAccountRepository) {
        this.savingsAccountRepository = savingsAccountRepository;
    }


    public void open(String idSavingsAccount) {
        guaranteeExistingSavingsAccount(idSavingsAccount);
        SavingsAccount mainAccount = savingsAccountRepository.getSavingAccount(idSavingsAccount);
        guaranteeNonExistingPocket(mainAccount);
        savingsAccountRepository.openPocket(idSavingsAccount);
    }

    private void guaranteeNonExistingPocket(SavingsAccount mainAccount) {
        if (savingsAccountRepository.existsPocket(mainAccount)) {
            throw new ExistingPocketException(EXISTING_POCKET_MESSAGE);
        }
    }

    private void guaranteeExistingSavingsAccount(String idSavingsAccount) {
        if (!savingsAccountRepository.existsSavingsAccount(idSavingsAccount)) {
            throw new NonExistingSavingsAccountException(NON_EXISTING_SAVINGS_ACCOUNT_MESSAGE);
        }
    }


}
