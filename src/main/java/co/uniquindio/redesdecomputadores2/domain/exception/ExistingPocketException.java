package co.uniquindio.redesdecomputadores2.domain.exception;

public class ExistingPocketException extends RuntimeException {
    public ExistingPocketException(String errorMessage) {
        super(errorMessage);
    }
}
