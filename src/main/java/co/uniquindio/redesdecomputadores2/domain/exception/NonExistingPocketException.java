package co.uniquindio.redesdecomputadores2.domain.exception;

public class NonExistingPocketException extends RuntimeException {
    public NonExistingPocketException(String errorMessage) {
        super(errorMessage);
    }

}
