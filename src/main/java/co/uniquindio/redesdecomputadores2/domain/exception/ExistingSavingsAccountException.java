package co.uniquindio.redesdecomputadores2.domain.exception;

public class ExistingSavingsAccountException extends RuntimeException {
    public ExistingSavingsAccountException(String errorMessage) {
        super(errorMessage);
    }
}
