package co.uniquindio.redesdecomputadores2.domain.exception;

public class NonExistingSavingsAccountException extends RuntimeException {
    public NonExistingSavingsAccountException(String errorMessage) {
        super(errorMessage);
    }

}
