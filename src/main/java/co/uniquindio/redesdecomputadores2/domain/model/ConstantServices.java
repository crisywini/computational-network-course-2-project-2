package co.uniquindio.redesdecomputadores2.domain.model;

public enum ConstantServices {
    ABRIR_CUENTA("ABRIR_CUENTA"),
    ABRIR_BOLSILLO("ABRIR_BOLSILLO"),
    CANCELAR_BOLSILLO("CANCELAR_BOLSILLO"),
    CANCELAR_CUENTA("CANCELAR_CUENTA"),
    DEPOSITAR("DEPOSITAR"),
    RETIRAR("RETIRAR"),
    TRASLADAR("TRASLADAR"),
    CONSULTAR("CONSULTAR"),
    CARGAR("CARGAR");

    private String description;

    ConstantServices(String description) {
        this.description = description;
    }

    String getDescription() {
        return this.description;
    }
}
