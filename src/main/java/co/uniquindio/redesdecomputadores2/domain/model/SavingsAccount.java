package co.uniquindio.redesdecomputadores2.domain.model;

import java.util.Objects;

public class SavingsAccount {
    private String id;
    private SavingsAccount pocket;
    private String userName;
    private String userLastName;
    private double balance;
    public SavingsAccount(String id, String userName, String userLastName, double balance) {
        this.id = id;
        this.userName = userName;
        this.userLastName = userLastName;
        this.balance = balance;
    }

    public SavingsAccount(String userName, String userLastName, double balance) {
        this.userName = userName;
        this.userLastName = userLastName;
        this.balance = balance;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SavingsAccount getPocket() {
        return pocket;
    }

    public void setPocket(SavingsAccount pocket) {
        this.pocket = pocket;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SavingsAccount that = (SavingsAccount) o;
        return userName.equals(that.userName) && userLastName.equals(that.userLastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, userLastName);
    }

    @Override
    public String toString() {
        return "SavingsAccount{" +
                "id='" + id + '\'' +
                ", pocket=" + pocket +
                ", userName='" + userName + '\'' +
                ", userLastName='" + userLastName + '\'' +
                ", balance=" + balance +
                '}';
    }
}
